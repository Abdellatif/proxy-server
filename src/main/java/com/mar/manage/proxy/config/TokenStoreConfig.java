package com.mar.manage.proxy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

@Configuration
public class TokenStoreConfig {
	private final String RESOURCE_NAME = "businesskey.jks";
	private final String FILE_PASSWORD = "BusinessPairKey";
	private final String FILE_ALIACE = "businesskey";
    
	@Bean
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        final JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        KeyStoreKeyFactory keyStoreKeyFactory = 
          new KeyStoreKeyFactory(new ClassPathResource(RESOURCE_NAME), FILE_PASSWORD.toCharArray());
        converter.setKeyPair(keyStoreKeyFactory.getKeyPair(FILE_ALIACE));
        return converter;
    }

    @Bean
    public TokenStore tokenStore(final JwtAccessTokenConverter tokenConverter) {
        return new JwtTokenStore(tokenConverter);
    }
}
