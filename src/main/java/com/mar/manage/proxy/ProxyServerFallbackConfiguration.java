package com.mar.manage.proxy;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;

import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;

@Configuration
public class ProxyServerFallbackConfiguration implements FallbackProvider {
	private String generateResponseBody(String route) {
		StringBuilder responseBodyBuilder = new StringBuilder();
		responseBodyBuilder.append("{\"message\":\"Service ");
		responseBodyBuilder.append(route);
		responseBodyBuilder.append(" is Unavailable. Please try later\"}");
		return responseBodyBuilder.toString();
	}
	
	// The fallbackResponse is applied for all routes
    @Override
    public String getRoute() {
        return "*";
    }

	@Override
	public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
		String responseBody = generateResponseBody(route);
		
        return new ClientHttpResponse() {
            @Override
            public HttpStatus getStatusCode() throws IOException {
				return HttpStatus.SERVICE_UNAVAILABLE;
            }
 
            @Override
            public int getRawStatusCode() throws IOException {
            	return HttpStatus.BAD_GATEWAY.value();
            }
 
            @Override
            public String getStatusText() throws IOException {
                return new String("Service Unavailable");
            }
 
            @Override
            public void close() {}
 
            @Override
            public InputStream getBody() throws IOException {
				return new ByteArrayInputStream(responseBody.getBytes());
            }
 
            @Override
            public HttpHeaders getHeaders() {
            	HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				return headers;
            }
        };
	}
}
