FROM openjdk:8-jre-alpine

ADD ./target/proxy-server.jar /app/
CMD ["java", "-Xmx200m", "-jar", "/app/proxy-server.jar"]

EXPOSE 9990
